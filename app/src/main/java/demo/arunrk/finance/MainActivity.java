package demo.arunrk.finance;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import demo.arunrk.finance.Utills.Prepare;
import demo.arunrk.finance.adapters.RecyclerBottomAdapter;
import demo.arunrk.finance.fragments.GraphFragmeant;
import demo.arunrk.finance.fragments.ListFragment;
import demo.arunrk.finance.fragments.WatchListFragment;
import demo.arunrk.finance.listeners.RecyclerItemClickListener;
import demo.arunrk.finance.pojo.Bottom;


public class MainActivity extends AppCompatActivity {


    private RecyclerView mRecyclerBottom;
    private RecyclerBottomAdapter bottomAdapter;
    private LinearLayoutManager mLayoutManager;
    List<Bottom> list;
    int page = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = new ArrayList<Bottom>();
        mRecyclerBottom = findViewById(R.id.mRecyclerBottom);
        mRecyclerBottom.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerBottom.setLayoutManager(mLayoutManager);
        list = Prepare.prepareBottom();
        bottomAdapter = new RecyclerBottomAdapter(this, list);
        mRecyclerBottom.setAdapter(bottomAdapter);

        mRecyclerBottom.addOnItemTouchListener(
                new RecyclerItemClickListener(this, mRecyclerBottom ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {

                        if (position == 0) {
                            page = 1;
                            setFragmentPage(WatchListFragment.newInstance(), "WatchListFragment");
                        } else {
                            page = 2;
                            getSupportFragmentManager().beginTransaction().replace(R.id.container, ListFragment.newInstance(), "ListFragment").commit();
                        }

                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        getSupportFragmentManager().beginTransaction().add(R.id.container, WatchListFragment.newInstance(), "WatchListFragment").commit();

    }

    private void setFragmentPage(Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, tag).commit();
    }

    @Override
    public void onBackPressed() {
        if (page == 1) {
            super.onBackPressed();
        } else {
            page = 1;
            setFragmentPage(WatchListFragment.newInstance(), "WatchListFragment");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        page = 1;
        setFragmentPage(WatchListFragment.newInstance(), "WatchListFragment");
    }
}
