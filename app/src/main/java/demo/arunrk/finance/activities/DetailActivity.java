package demo.arunrk.finance.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import demo.arunrk.finance.R;
import demo.arunrk.finance.fragments.GraphFragmeant;

public class DetailActivity extends AppCompatActivity {


    int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            try{
                position = bundle.getInt("position");
            }catch (Exception e) {
                position = 0;
            }
        } else {
            position = 0;
        }

        getSupportFragmentManager().beginTransaction().add(R.id.container, GraphFragmeant.newInstance(position), "GraphFragmeant").commit();
    }
}
