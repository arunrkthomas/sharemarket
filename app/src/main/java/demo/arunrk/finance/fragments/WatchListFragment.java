package demo.arunrk.finance.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import demo.arunrk.finance.R;
import demo.arunrk.finance.Utills.Prepare;
import demo.arunrk.finance.activities.DetailActivity;
import demo.arunrk.finance.adapters.RecyclerCurrencyVerticalAdapter;
import demo.arunrk.finance.adapters.RecyclerCurrencyHorizontalAdapter;
import demo.arunrk.finance.listeners.RecyclerItemClickListener;
import demo.arunrk.finance.pojo.Currency;


/**
 * A simple {@link Fragment} subclass.
 */
public class WatchListFragment extends Fragment {

    View view;
    private RecyclerView mRecyclerViewVertical, mRecyclerViewHorizontal;
    private RecyclerCurrencyVerticalAdapter adapterVertical;
    private RecyclerCurrencyHorizontalAdapter adapterHorizontal;
    private LinearLayoutManager mLayoutManagerVertical, mLayoutManagerHorizontal;
    List<Currency> listVertical, listHorizontal;
    private int draggingView = -1;
    int INTERVEL = 2000, INTERVELUPDATE = 1000, ask = 50, position = 0, bid = 60;
    Handler mHandler, mHandlerUpdate;
    Runnable mRunnable, mRunnableUpdate;

    public WatchListFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance() {
        return new WatchListFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_watch_list, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setView(view);
    }

    private void setView(View view) {
        mRecyclerViewVertical = (RecyclerView) view.findViewById(R.id.mRecyclerViewVertical);
        mRecyclerViewVertical.setHasFixedSize(true);
        mLayoutManagerVertical = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerViewVertical.setLayoutManager(mLayoutManagerVertical);
        DividerItemDecoration dividerItemDecorationVertical = new DividerItemDecoration(mRecyclerViewVertical.getContext(),
                mLayoutManagerVertical.getOrientation());
        mRecyclerViewVertical.addItemDecoration(dividerItemDecorationVertical);

        mRecyclerViewHorizontal = (RecyclerView) view.findViewById(R.id.mRecyclerViewHorizontal);
        mRecyclerViewHorizontal.setHasFixedSize(true);
        mLayoutManagerHorizontal = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerViewHorizontal.setLayoutManager(mLayoutManagerHorizontal);
        DividerItemDecoration dividerItemDecorationHorizontal = new DividerItemDecoration(mRecyclerViewHorizontal.getContext(),
                mLayoutManagerHorizontal.getOrientation());
        mRecyclerViewHorizontal.addItemDecoration(dividerItemDecorationHorizontal);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mHandler != null) mHandler.removeCallbacks(mRunnable);
        if (mHandlerUpdate != null) mHandlerUpdate.removeCallbacks(mRunnableUpdate);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listVertical = new ArrayList<Currency>();
        listHorizontal = new ArrayList<Currency>();

        listVertical = Prepare.prepareCurreny();
        listHorizontal = Prepare.prepareHorizontalCurreny();

        adapterVertical = new RecyclerCurrencyVerticalAdapter(getActivity(), listVertical);
        mRecyclerViewVertical.setAdapter(adapterVertical);

        adapterHorizontal = new RecyclerCurrencyHorizontalAdapter(getActivity(), listHorizontal);
        mRecyclerViewHorizontal.setAdapter(adapterHorizontal);

        adapterVertical.notifyDataSetChanged();
        adapterHorizontal.notifyDataSetChanged();
        mHandler = new Handler();
        mHandlerUpdate = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                position = Prepare.getRandomIndex();
                ask = Integer.parseInt(Prepare.getRandomNumber());
                bid = Integer.parseInt(Prepare.getRandomNumber());
                Currency currency = listHorizontal.get(position);
                currency.setAskMain(ask + "");
                currency.setBidMain(bid + "");
                currency.setAskBackground(ask >= 50 ? R.drawable.green : R.drawable.red);
                currency.setBidBackground(bid >= 50 ? R.drawable.green : R.drawable.red);
                adapterHorizontal.update(position, currency);
                mHandler.postDelayed(mRunnable, INTERVEL);
            }
        };

        mHandler.postDelayed(mRunnable, INTERVEL);

        mRunnableUpdate = new Runnable() {
            @Override
            public void run() {
                Currency currency = listHorizontal.get(position);
                currency.setAskMain(ask + "");
                currency.setBidMain(bid + "");
                currency.setAskBackground(R.drawable.primary);
                currency.setBidBackground(R.drawable.primary);
                adapterHorizontal.update(position, currency);
                mHandlerUpdate.postDelayed(mRunnableUpdate, INTERVELUPDATE);
            }
        };

        mHandlerUpdate.postDelayed(mRunnableUpdate, INTERVELUPDATE);


        syncScrollEvent(mRecyclerViewVertical, mRecyclerViewHorizontal);


        mRecyclerViewVertical.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), mRecyclerViewVertical, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        callActivity(position);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
        mRecyclerViewHorizontal.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), mRecyclerViewHorizontal, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        callActivity(position);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );


    }

    private void callActivity(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        startActivity(new Intent(getActivity(), DetailActivity.class).putExtras(bundle));
    }

    private void syncScrollEvent(final RecyclerView leftList, final RecyclerView rightList) {

        leftList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return rightList.getScrollState() != RecyclerView.SCROLL_STATE_IDLE;
            }
        });
        rightList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return leftList.getScrollState() != RecyclerView.SCROLL_STATE_IDLE;
            }
        });


        leftList.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (recyclerView.getScrollState() != RecyclerView.SCROLL_STATE_IDLE) {
                    rightList.scrollBy(dx, dy);
                }
            }
        });
        rightList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (recyclerView.getScrollState() != RecyclerView.SCROLL_STATE_IDLE) {
                    leftList.scrollBy(dx, dy);
                }
            }
        });

    }

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (mRecyclerViewVertical == recyclerView && newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                draggingView = 1;
            } else if (mRecyclerViewHorizontal == recyclerView && newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                draggingView = 2;
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (draggingView == 1 && recyclerView == mRecyclerViewVertical) {
                mRecyclerViewVertical.scrollBy(dx, dy);
            } else if (draggingView == 2 && recyclerView == mRecyclerViewHorizontal) {
                mRecyclerViewHorizontal.scrollBy(dx, dy);
            }
        }
    };


}