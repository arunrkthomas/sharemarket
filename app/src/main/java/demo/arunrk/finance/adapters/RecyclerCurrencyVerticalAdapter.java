package demo.arunrk.finance.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import demo.arunrk.finance.R;
import demo.arunrk.finance.pojo.Currency;
import android.support.v7.widget.RecyclerView;


/**
 * Created by ARUN on 8/25/2017.
 */

public class RecyclerCurrencyVerticalAdapter extends RecyclerView.Adapter<RecyclerCurrencyVerticalAdapter.DataObjectHolder> {

    Context context;
    List<Currency> list;

    public RecyclerCurrencyVerticalAdapter(Context context, List<Currency> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.currency_vertical_list, viewGroup, false);
        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.txtCurrency.setText(list.get(position).getCname());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView txtCurrency;

        public DataObjectHolder(View itemView) {
            super(itemView);
            txtCurrency = (TextView) itemView.findViewById(R.id.txtCurrency);
        }

    }
}
