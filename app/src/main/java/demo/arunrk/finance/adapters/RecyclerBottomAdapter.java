package demo.arunrk.finance.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import demo.arunrk.finance.R;
import demo.arunrk.finance.pojo.Bottom;
import demo.arunrk.finance.pojo.Currency;


/**
 * Created by ARUN on 8/25/2017.
 */

public class RecyclerBottomAdapter extends RecyclerView.Adapter<RecyclerBottomAdapter.DataObjectHolder> {

    Context context;
    List<Bottom> list;

    public RecyclerBottomAdapter(Context context, List<Bottom> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bottom_list, viewGroup, false);
        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.txtTitle.setText(list.get(position).getTitle());
        holder.txtCount.setText(list.get(position).getCount());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView txtTitle, txtCount;

        public DataObjectHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtCount = (TextView) itemView.findViewById(R.id.txtCount);
        }

    }
}
