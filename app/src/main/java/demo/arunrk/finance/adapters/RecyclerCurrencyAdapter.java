package demo.arunrk.finance.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import demo.arunrk.finance.R;
import demo.arunrk.finance.pojo.Currency;


/**
 * Created by ARUN on 8/25/2017.
 */

public class RecyclerCurrencyAdapter extends RecyclerView.Adapter<RecyclerCurrencyAdapter.DataObjectHolder> {

    Context context;
    List<Currency> list;

    public RecyclerCurrencyAdapter(Context context, List<Currency> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {

        holder.txtCurrency.setText(list.get(position).getCname());
        holder.txtBidSubL.setText(list.get(position).getBidSub());
        holder.txtBidMainL.setText(list.get(position).getBidMain());
        holder.txtBidSubR.setText(list.get(position).getAskSub());
        holder.txtBidMainR.setText(list.get(position).getAskMain());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView txtCurrency, txtBidSubL, txtBidSubR, txtBidMainL, txtBidMainR;

        public DataObjectHolder(View itemView) {
            super(itemView);

            txtCurrency = (TextView) itemView.findViewById(R.id.txtCurrency);
            txtBidSubL = (TextView) itemView.findViewById(R.id.txtBidSubL);
            txtBidSubR = (TextView) itemView.findViewById(R.id.txtBidSubR);
            txtBidMainL = (TextView) itemView.findViewById(R.id.txtBidMainL);
            txtBidMainR = (TextView) itemView.findViewById(R.id.txtBidMainR);

        }

    }
}
