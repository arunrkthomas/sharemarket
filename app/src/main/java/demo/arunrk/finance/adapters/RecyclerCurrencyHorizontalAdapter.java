package demo.arunrk.finance.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import java.util.List;

import demo.arunrk.finance.R;
import demo.arunrk.finance.pojo.Currency;
import android.support.v7.widget.RecyclerView;


/**
 * Created by ARUN on 8/25/2017.
 */

public class RecyclerCurrencyHorizontalAdapter extends RecyclerView.Adapter<RecyclerCurrencyHorizontalAdapter.DataObjectHolder> {

    Context context;
    List<Currency> list;
    int index = 0;
    private final static int FADE_DURATION = 1000;

    public RecyclerCurrencyHorizontalAdapter(Context context, List<Currency> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.currency_list_horizontal, viewGroup, false);
        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.txtAskMain.setBackgroundResource(list.get(position).getAskBackground());
        holder.txtAskSub.setBackgroundResource(list.get(position).getAskBackground());
        holder.txtBidMain.setBackgroundResource(list.get(position).getBidBackground());
        holder.txtBidSub.setBackgroundResource(list.get(position).getBidBackground());

        setFadeAnimation(holder.txtAskMain);
        setFadeAnimation(holder.txtAskSub);
        setFadeAnimation(holder.txtBidMain);
        setFadeAnimation(holder.txtBidSub);

        holder.txtAskSub.setText(list.get(position).getAskSub());
        holder.txtAskMain.setText(list.get(position).getAskMain());
        holder.txtOpenPrice.setText(list.get(position).getOpenPrice());
        holder.txtBidSub.setText(list.get(position).getBidSub());
        holder.txtBidMain.setText(list.get(position).getBidMain());
        holder.txtHigh.setText(list.get(position).getHigh());
        holder.txtLow.setText(list.get(position).getLow());
    }

    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(FADE_DURATION);
        view.startAnimation(anim);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void update(int randomIndex, Currency currency) {
        index = randomIndex;
        list.set(randomIndex, currency);
        notifyItemChanged(randomIndex);
    }

    @Override
    public void onViewRecycled(DataObjectHolder holder) {
        super.onViewRecycled(holder);
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView txtAskSub, txtAskMain, txtOpenPrice, txtBidSub, txtBidMain, txtHigh, txtLow;

        public DataObjectHolder(View itemView) {
            super(itemView);
            txtAskSub = (TextView) itemView.findViewById(R.id.txtAskSub);
            txtAskMain = (TextView) itemView.findViewById(R.id.txtAskMain);
            txtOpenPrice = (TextView) itemView.findViewById(R.id.txtOpenPrice);
            txtBidSub = (TextView) itemView.findViewById(R.id.txtBidSub);
            txtBidMain = (TextView) itemView.findViewById(R.id.txtBidMain);
            txtHigh = (TextView) itemView.findViewById(R.id.txtHigh);
            txtLow = (TextView) itemView.findViewById(R.id.txtLow);
        }

    }
}
