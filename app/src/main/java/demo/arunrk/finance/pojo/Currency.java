package demo.arunrk.finance.pojo;

/**
 * Created by ARUN on 8/25/2017.
 */

public class Currency {

    String askSub, askMain, openPrice, bidSub, bidMain, high, low;
    String cname, amt;
    int askBackground, bidBackground;

    public int getBidBackground() {
        return bidBackground;
    }

    public void setBidBackground(int bidBackground) {
        this.bidBackground = bidBackground;
    }

    public int getAskBackground() {
        return askBackground;
    }

    public void setAskBackground(int background) {
        this.askBackground = background;
    }

    public String getBidMain() {
        return bidMain;
    }

    public void setBidMain(String bidMain) {
        this.bidMain = bidMain;
    }

    public String getAskSub() {
        return askSub;
    }

    public void setAskSub(String askSub) {
        this.askSub = askSub;
    }

    public String getAskMain() {
        return askMain;
    }

    public void setAskMain(String askMain) {
        this.askMain = askMain;
    }

    public String getOpenPrice() {
        return openPrice;
    }

    public void setOpenPrice(String openPrice) {
        this.openPrice = openPrice;
    }

    public String getBidSub() {
        return bidSub;
    }

    public void setBidSub(String bidSub) {
        this.bidSub = bidSub;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }
}
