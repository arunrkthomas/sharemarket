package demo.arunrk.finance.pojo;

/**
 * Created by ARUN on 8/25/2017.
 */

public class Bottom {

    String title, count;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
