package demo.arunrk.finance.Utills;

import java.util.ArrayList;
import java.util.List;

import demo.arunrk.finance.R;
import demo.arunrk.finance.pojo.Bottom;
import demo.arunrk.finance.pojo.Currency;

/**
 * Created by arunrk on 26/8/17.
 */

public class Prepare {

    static List<Bottom> list;
    static Bottom bottom;
    static Currency currency;
    public static List<Currency> listVertical, listHorizontal;

    public static int getRandomIndex() {
        int minimum = 0, maximum = 14;
        return minimum + (int)(Math.random() * maximum);
    }

    public static String getRandomNumber(){
        int minimum = 10, maximum = 85;
        int random = minimum + (int)(Math.random() * maximum);
        System.out.println(random+" --- random number");
        return random + "";
    }

    public static List<Currency> prepareCurreny() {

        listVertical = new ArrayList<Currency>();
        currency = new Currency();
        currency.setCname("AUDUSD");
        listVertical.add(currency);

        currency = new Currency();
        currency.setCname("GBPUSD");
        listVertical.add(currency);

        currency = new Currency();
        currency.setCname("EURUSD");
        listVertical.add(currency);

        currency = new Currency();
        currency.setCname("USDCHF");
        listVertical.add(currency);

        currency = new Currency();
        currency.setCname("USDJPY");
        listVertical.add(currency);

        currency = new Currency();
        currency.setCname("EURGBP");
        listVertical.add(currency);

        currency = new Currency();
        currency.setCname("AUDJPY");
        listVertical.add(currency);

        currency = new Currency();
        currency.setCname("GBPJPY");
        listVertical.add(currency);

        currency = new Currency();
        currency.setCname("USDJPY");
        listVertical.add(currency);

        currency = new Currency();
        currency.setCname("USDCAD");
        listVertical.add(currency);

        currency = new Currency();
        currency.setCname("NZDUSD");
        listVertical.add(currency);

        currency = new Currency();
        currency.setCname("EURAUD");
        listVertical.add(currency);

        currency = new Currency();
        currency.setCname("GBPCHF");
        listVertical.add(currency);

        currency = new Currency();
        currency.setCname("GBPCAD");
        listVertical.add(currency);

        return listVertical;
    }

    public static List<Currency> prepareHorizontalCurreny() {

        listHorizontal = new ArrayList<Currency>();
        currency = new Currency();
        currency.setCname("AUDUSD");
        currency.setAskBackground(R.drawable.primary);
        currency.setBidBackground(R.drawable.primary);
        currency.setAskSub("0.79");
        currency.setAskMain("38");
        currency.setOpenPrice("0.7902");
        currency.setBidSub("109.");
        currency.setBidMain("30");
        currency.setHigh("0.7954");
        currency.setLow("0.7886");
        listHorizontal.add(currency);

        currency = new Currency();
        currency.setCname("GBPUSD");
        currency.setAskBackground(R.drawable.primary);
        currency.setBidBackground(R.drawable.primary);
        currency.setAskSub("1.28");
        currency.setAskMain("82");
        currency.setOpenPrice("1.2801");
        currency.setBidSub("1.28");
        currency.setBidMain("77");
        currency.setHigh("1.2889");
        currency.setLow("1.2795");
        listHorizontal.add(currency);

        currency = new Currency();
        currency.setCname("EURUSD");
        currency.setAskBackground(R.drawable.primary);
        currency.setBidBackground(R.drawable.primary);
        currency.setAskSub("1.19");
        currency.setAskMain("24");
        currency.setOpenPrice("1.1800");
        currency.setBidSub("1.19");
        currency.setBidMain("22");
        currency.setHigh("1.1942");
        currency.setLow("1.1773");
        listHorizontal.add(currency);

        currency = new Currency();
        currency.setCname("USDCHF");
        currency.setAskBackground(R.drawable.primary);
        currency.setBidBackground(R.drawable.primary);
        currency.setAskSub("0.95");
        currency.setAskMain("72");
        currency.setOpenPrice("0.9651");
        currency.setBidSub("0.95");
        currency.setBidMain("61");
        currency.setHigh("09663");
        currency.setLow("0.9550");
        listHorizontal.add(currency);

        currency = new Currency();
        currency.setCname("USDJPY");
        currency.setAskBackground(R.drawable.primary);
        currency.setBidBackground(R.drawable.primary);
        currency.setAskSub("109.");
        currency.setAskMain("39");
        currency.setOpenPrice("109.56");
        currency.setBidSub("109.");
        currency.setBidMain("35");
        currency.setHigh("109.85");
        currency.setLow("109.11");
        listHorizontal.add(currency);

        currency = new Currency();
        currency.setCname("EURGBP");
        currency.setAskBackground(R.drawable.primary);
        currency.setBidBackground(R.drawable.primary);
        currency.setAskSub("0.92");
        currency.setAskMain("64");
        currency.setOpenPrice("0.9218");
        currency.setBidSub("0.92");
        currency.setBidMain("56");
        currency.setHigh("0.9270");
        currency.setLow("0.9196");
        listHorizontal.add(currency);

        currency = new Currency();
        currency.setCname("AUDJPY");
        currency.setAskBackground(R.drawable.primary);
        currency.setBidBackground(R.drawable.primary);
        currency.setAskSub("130.");
        currency.setAskMain("38");
        currency.setOpenPrice("129.28");
        currency.setBidSub("130.");
        currency.setBidMain("35");
        currency.setHigh("130.44");
        currency.setLow("129.12");
        listHorizontal.add(currency);

        currency = new Currency();
        currency.setCname("GBPJPY");
        currency.setAskBackground(R.drawable.primary);
        currency.setBidBackground(R.drawable.primary);
        currency.setAskSub("86.0");
        currency.setAskMain("80");
        currency.setOpenPrice("86.56");
        currency.setBidSub("86.0");
        currency.setBidMain("72");
        currency.setHigh("86.98");
        currency.setLow("86.45");
        listHorizontal.add(currency);

        currency = new Currency();
        currency.setCname("USDJPY");
        currency.setAskBackground(R.drawable.primary);
        currency.setBidBackground(R.drawable.primary);
        currency.setAskSub("140.");
        currency.setAskMain("97");
        currency.setOpenPrice("140.23");
        currency.setBidSub("140.");
        currency.setBidMain("78");
        currency.setHigh("140.86");
        currency.setLow("140.15");
        listHorizontal.add(currency);

        currency = new Currency();
        currency.setCname("USDCAD");
        currency.setAskBackground(R.drawable.primary);
        currency.setBidBackground(R.drawable.primary);
        currency.setAskSub("1.24");
        currency.setAskMain("82");
        currency.setOpenPrice("1.2514");
        currency.setBidSub("1.24");
        currency.setBidMain("77");
        currency.setHigh("1.2540");
        currency.setLow("1.2466");
        listHorizontal.add(currency);

        currency = new Currency();
        currency.setCname("NZDUSD");
        currency.setAskBackground(R.drawable.primary);
        currency.setBidBackground(R.drawable.primary);
        currency.setAskSub("0.72");
        currency.setAskMain("46");
        currency.setOpenPrice("0.7208");
        currency.setBidSub("0.72");
        currency.setBidMain("38");
        currency.setHigh("0.7259");
        currency.setLow("0.7198");
        listHorizontal.add(currency);

        currency = new Currency();
        currency.setCname("EURAUD");
        currency.setAskBackground(R.drawable.primary);
        currency.setBidBackground(R.drawable.primary);
        currency.setAskSub("1.50");
        currency.setAskMain("38");
        currency.setOpenPrice("1.4932");
        currency.setBidSub("1.50");
        currency.setBidMain("19");
        currency.setHigh("1.5036");
        currency.setLow("1.4884");
        listHorizontal.add(currency);

        currency = new Currency();
        currency.setCname("GBPCHF");
        currency.setAskBackground(R.drawable.primary);
        currency.setBidBackground(R.drawable.primary);
        currency.setAskSub("1.23");
        currency.setAskMain("22");
        currency.setOpenPrice("1.2354");
        currency.setBidSub("1.23");
        currency.setBidMain("12");
        currency.setHigh("1.2382");
        currency.setLow("1.2297");
        listHorizontal.add(currency);

        currency = new Currency();
        currency.setCname("GBPCAD");
        currency.setAskBackground(R.drawable.primary);
        currency.setBidBackground(R.drawable.primary);
        currency.setAskSub("1.60");
        currency.setAskMain("81");
        currency.setOpenPrice("1.6019");
        currency.setBidSub("1.60");
        currency.setBidMain("66");
        currency.setHigh("1.6103");
        currency.setLow("1.6007");
        listHorizontal.add(currency);

        return listHorizontal;
    }
    public static List<Bottom> prepareBottom() {

        list = new ArrayList<Bottom>();
        bottom = new Bottom();
        bottom.setTitle("Watchlist");
        bottom.setCount("(14)");
        list.add(bottom);
        bottom = new Bottom();
        bottom.setTitle("WLAdvanced");
        bottom.setCount("(14)");
        list.add(bottom);
        bottom = new Bottom();
        bottom.setTitle("Open");
        bottom.setCount("");
        list.add(bottom);
        bottom = new Bottom();
        bottom.setTitle("Working");
        bottom.setCount("");
        list.add(bottom);
        bottom = new Bottom();
        bottom.setTitle("Settled");
        bottom.setCount("");
        list.add(bottom);
        bottom = new Bottom();
        bottom.setTitle("Cancelled");
        bottom.setCount("");
        list.add(bottom);
        bottom = new Bottom();
        bottom.setTitle("Executed");
        bottom.setCount("");
        list.add(bottom);
        bottom = new Bottom();
        bottom.setTitle("Accounts");
        bottom.setCount("");
        list.add(bottom);
        bottom = new Bottom();
        bottom.setTitle("News");
        bottom.setCount("");
        list.add(bottom);
        bottom = new Bottom();
        bottom.setTitle("Chat");
        bottom.setCount("");
        list.add(bottom);
        bottom = new Bottom();
        bottom.setTitle("Message");
        bottom.setCount("");
        list.add(bottom);
        bottom = new Bottom();
        bottom.setTitle("Settings");
        bottom.setCount("");
        list.add(bottom);
        bottom = new Bottom();
        bottom.setTitle("Logout");
        bottom.setCount("");
        list.add(bottom);

        return list;
    }


}
